# Wardrobify

Team:

* Andrew Chung - Shoes
* Person 2 - Not available

## Design

MVP product with slight stlying on the main page.

## Shoes microservice

You can pull Django shoe API application from the browser or Insomnia using port 8080. Shoes has two microservices: shoes/api and shoes/poll.
    Pulled the bin data from Wardrobe to designate specific shoes into specific bins

    Shoes form and list is created listing out manufacturer, model, color, URL, and which bin it belongs.

    Once Shoe Form is complete, the page should refresh automatically and display on the Shoe List. Delete feature has been added on Shoe List to remove any shoes listed.

Model: // Pulled Bin from Wardrobe to create BinVO to pull bin number, import_href // Shoe model created with manufacture, model_name, color, picture_url, and bin foreignkey.



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
