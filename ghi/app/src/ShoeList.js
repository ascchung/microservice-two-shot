export default function ShoeList({ shoes, getShoes }) {
    const deleteShoe = async (id) => {
        try {
            await fetch(`http://localhost:8080/api/shoes/${id}/`, {
                method: "delete",
            });

            // Assuming getShoes is a function that returns a Promise
            await getShoes();
        } catch (error) {
            console.error(error);
        }
    }

    if (shoes === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td><img src={ shoe.picture_url } alt="" width= '100' height= '100'></img></td>
                            <td>{ shoe.bin_number }</td>
                            <td><button onClick={() => deleteShoe(shoe.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
};
