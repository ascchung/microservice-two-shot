import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

export default function App() {

  const [ shoes, setShoes ] = useState([]);

  async function getShoes() {
    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const shoeResponse = await fetch(shoeUrl);
    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes);
    }
  }

  useEffect(() => {
    getShoes();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ShoeList shoes={shoes} getShoes={getShoes} />} />
            <Route path="new" element={<ShoeForm getShoes={getShoes} />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}
