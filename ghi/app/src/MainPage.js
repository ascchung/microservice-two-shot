import React from 'react';
import { Link } from 'react-router-dom';

export default function MainPage() {
  return (
    <div className="main-page">
        <div className="container py-5 text-center">
          <h1 className="display-4 fw-bold text-dark">WARDROBIFY!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4 text-blue">
              Need to keep track of your shoes and hats? We have the solution for you!
            </p>
            <Link to="/shoes" className="btn btn-primary btn-lg">
              Check Your Shoe Collection!
            </Link>
            <Link to="/hats" className="btn btn-primary btn-lg ms-3">
              Check Your Hat Collection!
            </Link>
          </div>
        </div>
      </div>
  );
}
