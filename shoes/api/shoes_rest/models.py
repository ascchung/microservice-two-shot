from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT, #better than CASCADE, if accident almost delete bin
        null=True
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
